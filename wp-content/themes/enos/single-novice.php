<?php
/**
 * The template for displaying all single posts.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<section style="margin-bottom: 30px; padding-bottom: 80px;   padding-top: 100px; background: url(<?php echo get_the_post_thumbnail_url(); ?>)"" class="mbr-section content5 cid-rRlfEj4TQa mbr-parallax-background" id="content5-s">


    <div class="mbr-overlay" style="opacity: 0.3; background-color: #000000;">
    </div>

    <div class="container">
        <div class="media-container-row">
            <div class="title col-12 col-md-12">
                <h2 class="align-center mbr-bold mbr-white pb-3 mbr-fonts-style display-1">
                    <?php the_title(); ?>
										</h2>


            </div>
        </div>
    </div>
</section>

			
<section style="background: white;" class="">
	<div class="container">
		<div class="row pb-5">
			
			<?php 
			// get 3 latest news
			$args = array(
					'post_type'        => 'novice',
					'posts_per_page'   => 10,
			);
	
			$novice = new WP_Query( $args );
			?>
		
			<div class="col-lg-4 col-12 pr-5">
				
						<h2 style=" font-weight: 500;
    margin-bottom: 0;
    text-align: left; font-family: 'Rubik', sans-serif;
    font-size: 1.5rem;
    padding-top: 0rem; margin-bottom: 2rem;">Zadnje novice</h2>
						
						<?php if($novice->posts): ?>
						<ul>
							<?php foreach($novice->posts as $p): ?>
								<li><a style="color: black; font-family: 'Rubik', sans-serif;" href="<?php echo get_permalink($p->ID); ?>"><?php echo $p->post_title; ?></a></li>
							<?php endforeach; ?>
						</ul>
						<?php endif; ?>
						
			</div>

			<div class="col-lg-8 col-12">
				<div class="row">
							
					<?php while ( have_posts() ) : the_post(); ?>
                    <div class="col-12 mt-3">
                        <?php the_content(); ?>
                    </div>
					<?php endwhile; // end of the loop. ?>
				</div>
			</div>

		</div>
	</div>
</section>
		


<!-- contact bottom -->
<?php get_template_part("/template-parts/contact_bottom"); ?>
<!-- contact bottom -->


<?php get_footer(); ?>
