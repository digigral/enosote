<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
setlocale(LC_ALL, "sl_SI");
$mydate = getdate(date("U"));
$args = array(
    'post_type' => 'ceniki',
    'orderby' => 'date',
    'order'   => 'DESC',
    'tax_query' => array(
        array(
            'taxonomy' => 'leto',
            'field' => 'slug',
            'terms' => $mydate[year],
        ),
    ),
);
$ceniki = new WP_Query($args);

$terms = get_terms( array(
    'taxonomy' => 'leto',
    'hide_empty' => true,
    'orderby' => 'name',
    'order' => 'DESC' 
) );

?>



<div class="row">
    <div class="pl-6 col-3 mb-5" style="padding-top: 50px">
        <div class="sidebar-navigation">
            <h2>Cenik</h2>
            <ul>
                <?php
                if($ceniki->posts):
                    foreach ($ceniki->posts as $cenik):
                        ?>
                        <li>
                            <a class="side-link-cenik <?php if(strpos($cenik->post_title, $mydate[month]) !== false){ echo 'active';} ?>"  data-id="<?php echo '#' . $cenik->ID ; ?>"> <?php echo $cenik->post_title; ?></a>
                        </li>
                    <?php endforeach;
                endif;
                ?>
            </ul>
            <h2>Arhiv cenikov</h2>
            <ul>
                <?php
                if($terms):
                    foreach ($terms as $term):
                        if($term->name != $mydate[year]):
                        ?>
                        <li>
                            <a class="side-link-cenik" href="/leto/<?php echo $term->slug; ?>"> <?php echo $term->name; ?></a>
                        </li>
                    <?php endif;
                        endforeach;
                endif;
                ?>
            </ul>
        </div>
    </div>
    <div class="col-md-9 col-12 content mb-5" style="min-height: 600px">
        <?php
        if($ceniki->posts):
            foreach ($ceniki->posts as $key=>$cenik):
                ?>
                <div class="cenik-warp" id="<?php echo $cenik->ID ?>" style="<?php if($key>0){ echo 'display:none';} ?>">
                    <h2><?php echo $cenik->post_title; ?></h2>
                    <div class="vsebina">
                        <div class="vsebina-zgoraj">
                            <?php echo get_field('vsebina_zgoraj', $cenik->ID) ?>
                        </div>

                        <?php
                        $tabela_naslov  = get_field('naslovi_tabel', $cenik->ID);   //d($tabela_naslov);
                        $tabela_vsebina  = get_field('vsebina_tabele', $cenik->ID); //d($tabela_vsebina);
                        ?>

                        <!-- cenik template -->
                        <section style="<?php if( $cenik_conunter == 0 ): ?> padding-top: 30px;  padding-bottom: 45px; <?php else: ?> padding-top: 0px;  padding-bottom: 60px; <?php endif; ?>" class="section-table cid-rTqk06IQgf" id="table1-3f">
                            <div class="container container-table">
                                <div class="table-wrapper">
                                    <div class="container scroll">
                                        <table class="table" cellspacing="0">
                                            <thead>
                                            <tr class="table-heads ">
                                                <?php if($tabela_naslov):  ?>
                                                    <?php foreach($tabela_naslov as $n): ?>
                                                        <th class="head-item mbr-fonts-style display-7">
                                                            <?php echo $n['tekst']; ?>
                                                        </th>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </tr>
                                            </thead>
                                            <?php if($tabela_vsebina):  ?>
                                                <tbody>
                                                <?php foreach($tabela_vsebina as $t):  ?>
                                                    <tr>
                                                        <?php foreach(  $t['vrstice'] as $t1  ): ?>
                                                            <td class="body-item mbr-fonts-style display-7"><?php echo $t1['tekst']; ?></td>
                                                        <?php endforeach; ?>
                                                    </tr>
                                                <?php endforeach; ?>
                                                </tbody>
                                            <?php endif; ?>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <?php $cenik_conunter = $cenik_conunter + 1; ?>
                        <div class="vsebina-spodaj">
                            <?php echo get_field('vsebina_spodaj', $cenik->ID) ?>
                        </div>
                    </div>
                </div>
            <?php endforeach;
        endif;
        ?>

    </div>
</div>

<!-- contact bottom -->
<?php get_template_part("/template-parts/contact_bottom"); ?>
<!-- contact bottom -->


<?php get_footer(); ?>

