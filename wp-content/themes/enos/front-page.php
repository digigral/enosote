<?php
// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
get_header();
?>


<section class="header1">
    <div class="header-slider">
    <?php $slider = get_field('hero_sekcija_slike');
    if($slider):
        foreach ($slider as $item):
    ?>
            <div>
                <?php if($item['povezava']): ?>
                <a href="<?php echo $item['povezava']; ?>" target="_blank">
                <?php endif; ?>
                    <div style="padding-top: 180px; background-size: cover; background: url(<?php echo $item['hero_sekcija_copy']; ?>" class="cid-rReVOLM4ek mbr-parallax-background" >
                        <div class="mbr-overlay">
                        </div>

                        <div class="container">
                            <div class="row justify-content-md-center">
                                <div class="mbr-white col-md-10">
                                    <h3 class="mbr-section-subtitle align-center mbr-light pb-3 mbr-fonts-style display-2">
                                        <?php echo $item["hero_naslov"]; ?></h3>
                                    <p class="mbr-text align-center pb-3 mbr-fonts-style display-5">
                                        <?php echo $item["hero_podnaslov"]; ?>
                                    </p>
                                    <?php if($item['povezava']): ?>
                                    <div class="read-more-warp">
                                        <div class="read-more align-center"> VEČ </div>
                                    </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if($item['povezava']): ?>
                </a>
            <?php endif; ?>
            </div>
    <?php
        endforeach;
    endif;
    ?>
    </div>
</section>


<section  style="padding-bottom: 40px;" class="services1 cid-rReZYgODfk" id="services1-c">
    <!---->

    <!---->
    <!--Overlay-->

    <!--Container-->
    <div class="container">
        <div class="row justify-content-center">
            <!--Titles-->
            <div class="title pb-5 col-12">
                <h2 class="align-left pb-3 mbr-fonts-style display-1"><?php echo get_field("energetske_resitve_-_naslov"); ?></h2>
                <h3 style="text-align: center;" class="mbr-section-subtitle mbr-light mbr-fonts-style display-5">
                  <?php echo get_field("energetske_resitve_podnaslov"); ?>
                </h3>
            </div>

            <?php $items_resitve = get_field("energetske_resitve_postavke"); ?>

            <?php if($items_resitve): ?>
            <?php foreach($items_resitve as $r): ?>
            <!--single-->
            <a href="<?php echo $r['povezava']; ?>">
            <div style="margin-bottom: 2rem;" class="card col-12 col-md-6 p-3 col-lg-4">
                <div class="card-wrapper">
                    <div class="card-img">
                        <img src="<?php echo $r['slika']; ?>" alt="" title="">
                    </div>
                    <div class="card-box">
                        <h4 style="color: black;" class="card-title mbr-fonts-style display-5"><?php echo $r['naslov']; ?></h4>
                        <p class="mbr-text mbr-fonts-style display-7">
                          <?php echo $r['povezetek']; ?>
                          <br>
                        </p>
                        <!--Btn-->
                        <div class="mbr-section-btn align-left"><a href="<?php echo $r['povezava']; ?>" class="btn btn-warning-outline display-4">Preverite</a></div>
                    </div>
                </div>
            </div>
            </a>
            <!--single-->
            <?php endforeach; ?>
            <?php endif; ?>

        </div>
    </div>
</section>




<?php get_template_part('template-parts/flexible-content'); ?>


<!-- contact bottom -->
<?php get_template_part("/template-parts/contact_bottom"); ?>
<!-- contact bottom -->


<?php get_footer(); ?>
