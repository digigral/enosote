(function ($) {
    function initMap() {
        var myMarkers = [
            {ime: 'CNG polnilnica LPP',link: 'https://goo.gl/maps/zBfH93LMqzqMneNR9', loc: {lat: 46.077944, lng: 14.488264}},
            {ime: 'CNG Polnilnica P+R Dolgi most',link: 'https://goo.gl/maps/1oRKeGp1mY87SRWz5',  loc: {lat: 46.039623, lng: 14.463659}},
            {ime: 'Metan Jesenice', link: 'https://goo.gl/maps/fMgmj7FnZueenjpy9 ', loc: {lat: 46.430708, lng: 14.064491}},
            {ime: 'CNG- Energetika Maribor', link:'https://goo.gl/maps/raEMd5wv6awhW3zJ7', loc: {lat: 46.533918, lng: 15.660176}},
            {ime: 'CNG station Celje',link: 'https://goo.gl/maps/tHRRHdpwoPpUp2jR8', loc: {lat: 46.239806, lng: 15.290062}}];

        var map = new google.maps.Map(document.getElementById('gmaps'), {
            zoom: 8,
            center: {lat: 46.040848, lng: 14.531027}
        });
        var activeInfoWindow;
        $.each(myMarkers, function (index, value) {
            var marker = new google.maps.Marker({
                position: value.loc,
                map: map,
            });
            var contentString = '<h4>'+value.ime+'</h4>'+
                '<a href="'+ value.link +'" target="_blank">navodila za pot</a>';
            var infowindow = new google.maps.InfoWindow({
                content: contentString
            });
            marker.addListener('click', function() {
                console.log('works');
                if (activeInfoWindow) { activeInfoWindow.close();}
                infowindow.open(map, marker);
                activeInfoWindow = infowindow;


                // $('html, body').animate({
                //     scrollTop: $('div#'+value.ime).offset().top
                // }, 1000);
            });
        });


    }

    $(document).ready(function () {
        console.log("loc doc ready");
        initMap();
    });

})(jQuery);
