<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
setlocale(LC_ALL, "sl_SI");
$mydate = getdate(date("U"));

$ceniki = new WP_Query($args);

$terms = get_terms( array(
    'taxonomy' => 'leto',
    'hide_empty' => true,
) );
$cterm = get_queried_object();

?>

<?php get_template_part('template-parts/header_image'); ?>

<div class="row">
    <div class="pl-6 col-3 mb-5" style="padding-top: 50px">
        <div class="sidebar-navigation">
            <h2>Arhiv cenikov</h2>
            <ul>
                <?php
                if($terms):
                    foreach ($terms as $term):
                        if($term->name != $mydate[year]):
                            ?>
                            <li>
                                <a class="side-link-cenik <?php if($cterm->slug == $term->slug){ echo 'active';} ?>" href="/leto/<?php echo $term->slug; ?>"> <?php echo $term->name; ?></a>
                            </li>
                        <?php else: ?>
                            <li>
                                <a class="side-link-cenik" href="/ceniki"> <?php echo $mydate[year] ?></a>
                            </li>
                        <?php endif;
                    endforeach;
                endif;
                ?>
            </ul>
        </div>
    </div>
    <div class="col-md-9 col-12 content mb-5" style="min-height: 600px">
        <ul class="pt-5">
            <?php
            if ( have_posts() ) : ?>
                <?php /* Start the Loop */ ?>
                <?php while ( have_posts() ) : the_post();

                    ?>
                <li>
                    <a href="<?php echo get_permalink($post->ID); ?>" <h2><?php echo $post->post_title; ?></h2>
                </li>
                <?php endwhile;
            endif;
            ?>
        </ul>

    </div>
</div>

<!-- contact bottom -->
<?php get_template_part("/template-parts/contact_bottom"); ?>
<!-- contact bottom -->


<?php get_footer(); ?>

