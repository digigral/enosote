<?php

/**
 *
 *  Custom Post Type registration
 *
 */

function register_my_posttypes()
{

  /*
   *
   * example cpt
   * replace example with your own cpt labels
	 * replace digi with your own text domain
   */

  $labels = array(
      'name'               => _x( 'Novice', 'post type general name', 'digi' ),
      'singular_name'      => _x( 'Novice', 'post type singular name', 'digi' ),
      'menu_name'          => _x( 'Novice', 'admin menu', 'digi' ),
      'name_admin_bar'     => _x( 'Novice', 'add new on admin bar', 'mdigi' ),
      'add_new'            => _x( 'Add new', 'slider', 'digi' ),
      'add_new_item'       => __( 'Add new item', 'digi' ),
      'new_item'           => __( 'New Novice', 'digi' ),
      'edit_item'          => __( 'Edit Novice', 'digi' ),
      'view_item'          => __( 'View Novice', 'digi' ),
      'all_items'          => __( 'All Novice', 'digi' ),
      'search_items'       => __( 'Search Novice', 'digi' ),
      'parent_item_colon'  => __( 'Parent Novice:', 'digi' ),
      'not_found'          => __( 'Not found', 'digi' ),
      'not_found_in_trash' => __( 'Not found in trash', 'digi' )
  );

  $args = array(
      'labels'             => $labels,
      'public'             => true,
      'publicly_queryable' => true,
      'show_ui'            => true,
      'show_in_menu'       => true,
      'query_var'          => true,
      'rewrite'            => array( 'slug' => 'novice', 'with_front' => true ),
      'capability_type'    => 'post',
      'has_archive'        => true,
      'taxonomies' => array('post_tag'),
      'hierarchical'       => false,
      'menu_position'      => null,
      'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'revisions' )
  );

  register_post_type( 'novice', $args );
    register_taxonomy(
        'kategorija',
        'novice',
        array(
            'rewrite' => array(
                "slug" => 'kategorija'
            ),
            'label' => __("Kategorija"),
            'hierarchical' => true
        )
    );
    register_taxonomy_for_object_type( 'kategorija', 'novice' );

    $labels = array(
        'name'               => _x( 'Ceniki', 'post type general name', 'digi' ),
        'singular_name'      => _x( 'Ceniki', 'post type singular name', 'digi' ),
        'menu_name'          => _x( 'Ceniki', 'admin menu', 'digi' ),
        'name_admin_bar'     => _x( 'Ceniki', 'add new on admin bar', 'mdigi' ),
        'add_new'            => _x( 'Add new', 'slider', 'digi' ),
        'add_new_item'       => __( 'Add new item', 'digi' ),
        'new_item'           => __( 'New Ceniki', 'digi' ),
        'edit_item'          => __( 'Edit Ceniki', 'digi' ),
        'view_item'          => __( 'View Ceniki', 'digi' ),
        'all_items'          => __( 'All Ceniki', 'digi' ),
        'search_items'       => __( 'Search Ceniki', 'digi' ),
        'parent_item_colon'  => __( 'Parent Ceniki:', 'digi' ),
        'not_found'          => __( 'Not found', 'digi' ),
        'not_found_in_trash' => __( 'Not found in trash', 'digi' )
    );

    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'ceniki', 'with_front' => true ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'taxonomies' => array('post_tag'),
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'revisions' )
    );

    register_post_type( 'ceniki', $args );

    register_taxonomy(
        'leto',
        'ceniki',
        array(
            'rewrite' => array(
                "slug" => 'leto'
            ),
            'label' => __("Leto"),
            'hierarchical' => true
        )
    );
    register_taxonomy_for_object_type( 'leto', 'ceniki' );
}

add_action( 'init', 'register_my_posttypes' );
