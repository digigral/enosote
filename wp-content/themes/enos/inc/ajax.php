<?php

function locations_callback() {
  $marker_img_url = "/wp-content/themes/fersped/img/contact-position.png";
  $location_array = [];
  $return_array = [];

  $args = array(
    'post_type'              => array( 'lokacije' ),
    'posts_per_page'         => '-1',
  );

  $query = new WP_Query( $args );

  if ( $query->have_posts() ) {
    while ( $query->have_posts() ) {
      $query->the_post();
      $location_array[get_the_title()] = get_fields();
    }
  }

  wp_reset_postdata();

  $return_array['marker'] = $marker_img_url;
  $return_array['locations'] = $location_array;

  echo json_encode($return_array);
	die();
}

add_action('wp_ajax_locations', 'locations_callback');
add_action('wp_ajax_nopriv_locations', 'locations_callback');
