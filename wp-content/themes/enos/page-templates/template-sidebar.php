<?php
/**
 * Template Name: sidebar
 *
 * This template can be used to override the default template and sidebar setup
 *
 * @package understrap
 */
// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
?>


<?php get_template_part('template-parts/header_image'); ?>

<style>

    .cid-rRvUm4Ifkq .nav-tabs .nav-link {
        font-weight: 500;
        font-family: 'Rubik', sans-serif;
        font-size: 1rem;
        color: #000000 !important;
        padding-bottom: 0 !important;
    }

    .tabs1  {

        padding-top: 10px !important;
        padding-bottom: 10px !important;

    }

</style>
<div class="row">
    <div class="pl-6 col-3" style="padding-top: 50px">
        <div class="sidebar-navigation">
            <h2><?php echo get_field('naslov'); ?></h2>
            <ul>
                <?php
                $programi = get_field('sidebar_programi');
                if($programi):
                    foreach ($programi as $program):
                        ?>
                        <li>
                            <a class="side-link" href="<?php echo $program['link_programa']; ?>"> <?php echo $program['ime_programa']; ?></a>
                        </li>
                    <?php endforeach;
                endif;
                ?>
            </ul>
        </div>
    </div>
    <div class="col-md-9 col-12">
        <?php if (have_rows('sekcije_prodajni_program')): ?>

            <?php while (have_rows('sekcije_prodajni_program')) : the_row(); ?>

                <?php if (get_row_layout() == 'posamezni_prodajni_program'): ?>

                    <?php
                    $id =  get_sub_field('id_sekcije');
                    $naslov = get_sub_field('naslov_programa');
                    $tekst = get_sub_field('tekst');
                    $toogle = get_sub_field('izdelki');
                    //d($toogle);
                    ?>

                    <!-- on storitev -->

                    <section style="padding-top: 50px;" class="toggle2 cid-rRli6S1k6P <?php echo $id;  ?>" id="toggle2-16">
                        <div class="offset-id" id="<?php echo $id;  ?>" ></div>
                        <div class="container">
                            <div class="media-container-row">
                                <div class="toggle-content">

                                    <h2 class="mbr-section-title pb-3 align-left mbr-fonts-style display-2">
                                        <?php echo $naslov;  ?>
                                    </h2>
                                    <h3 class="mbr-section-subtitle align-left mbr-fonts-style display-7">
                                        <?php echo $tekst;  ?>
                                    </h3>

                                    <?php if($toogle): ?>
                                        <div style="padding-top: 2rem!important;" id="bootstrap-toggle"  class="toggle-panel accordionStyles tab-content pt-5 mt-2">

                                            <?php foreach ($toogle as $idkey=>$t): ?>
                                                <!-- one toggle -->
                                                <div class="card">
                                                    <div class="card-header" role="tab" id="headingOne">
                                                        <a role="button"   class=" panel-title text-black"   data-toggle="collapse" data-core="" href="#acc-<?php echo $t['id_izdelka_']; ?>" >
                                                            <h4 class="mbr-fonts-style display-7">
                                           <span class="sign mbr-iconfont mbri-arrow-down inactive">

                                           </span>
                                                                <?php echo $t['naziv_izdelka']; ?>
                                                            </h4>
                                                        </a>
                                                    </div>
                                                    <div id="acc-<?php echo $t['id_izdelka_']; ?>" class="panel-collapse noScroll collapse " role="tabpanel" aria-labelledby="headingOne">
                                                        <div class="panel-body p-4">

                                                            <div class="row">
                                                                <div class="col-12 col-md-8">
                                                                    <div class="mbr-fonts-style panel-text display-7">
                                                                        <?php echo $t['opis_izdelka']; ?>



                                                                        <section class="tabs1 cid-rRvUm4Ifkq" id="extTabs2-22">
                                                                            <div class="container2">
                                                                                <?php
                                                                                $zavihki = $t['tabs'];
                                                                                if($zavihki):
                                                                                    ?>
                                                                                    <div class="row">
                                                                                        <div class="col-12 col-md-12">
                                                                                            <ul class="nav nav-tabs">
                                                                                                <?php
                                                                                                foreach ($zavihki as $key=>$item):
                                                                                                    ?>
                                                                                                    <li class="nav-item  <?php if ($key == 0 ){echo 'first active'; } ?>">
                                                                                                        <a class="nav-link mbr-fonts-style  <?php if ($key == 0 ){echo 'show active';} ?> display-5" role="tab" data-toggle="tab" href="#tab<?php echo $id . $idkey . $key?>" >
                                                                                                            <?php echo $item['naslov_zavihka'] ?>
                                                                                                        </a>
                                                                                                    </li>
                                                                                                <?php endforeach;
                                                                                                ?>
                                                                                            </ul>
                                                                                            <div class="tab-content">
                                                                                                <?php foreach ($zavihki as $key=>$item): ?>
                                                                                                    <div id="tab<?php echo $id . $idkey . $key?>" class="tab-pane <?php if ($key == 0 ){echo 'fade in active show';} else {echo 'fade in';} ?>" role="tabpanel">
                                                                                                        <div class="row">
                                                                                                            <div class="col-md-12">
                                                                                                                <p class="mbr-text pt-4 mbr-fonts-style display-4">
                                                                                                                    <?php echo $item['vsebina_zavihka'] ?>
                                                                                                                </p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                <?php endforeach; ?>
                                                                                            </div>
                                                                                            <div class="eko-sklad">
                                                                                                <div>Uredimo dokumentacijo za pridobitev nepovratnih sredstev in/ali kredita <a href="https://www.ekosklad.si/prebivalstvo/pridobite-spodbudo/seznam-spodbud/toplotne-crpalke" target="_blank"><strong>Eko sklada j.s.</strong></a></div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                <?php endif; ?>
                                                                            </div>
                                                                        </section>

                                                                        <a style="margin-left: 0;" class="btn btn-sm btn-primary display-4 izdelek-pov" href="#povprasevanje" data-name="<?php echo $t['naziv_izdelka']; ?>"><span class="fa fa-product-hunt mbr-iconfont mbr-iconfont-btn"></span>
                                                                            Pošlji povpraševanje</a>

                                                                    </div>

                                                                </div>
                                                                <div class="col-12 col-md-4">
                                                                    <?php $slike = $t['slike_izdelka'];
                                                                    if ($slike):
                                                                        ?>

                                                                        <div class="predstavitvena">
                                                                            <img class="predstavitvena-img" src="<?php echo $slike[0]['url']; ?>" />
                                                                        </div>
                                                                        <div class="row px-3 pt-3">
                                                                            <?php
                                                                            foreach ($slike as $item):
                                                                                ?>
                                                                                <div class="col-4 px-1" >
                                                                                    <div class="gallery-wrap">
                                                                                        <img class="gallery-image" src="<?php echo $item['url']; ?>"  alt="">
                                                                                    </div>
                                                                                </div>
                                                                            <?php
                                                                            endforeach;
                                                                            ?>
                                                                        </div>
                                                                    <?php
                                                                    endif;
                                                                    ?>
                                                                </div>
                                                            </div>


                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- one toggle -->
                                            <?php endforeach; ?>


                                        </div>
                                    <?php endif; ?>

                                </div>


                            </div>
                        </div>
                    </section>
                    <!-- on storitev -->

                <?php endif; ?>
                <!-- druga -->


            <?php endwhile; ?>
        <?php endif; ?>
    </div>
</div>

<!-- contact bottom -->
<?php get_template_part("/template-parts/contact_bottom"); ?>
<!-- contact bottom -->


<?php get_footer(); ?>
