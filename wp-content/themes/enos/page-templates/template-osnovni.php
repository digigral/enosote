<?php
/**
 * Template Name: Osnovni template
 *
 * This template can be used to override the default template and sidebar setup
 *
 * @package understrap
 */
// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
?>

<?php get_template_part('template-parts/flexible-content'); ?>

<!-- contact bottom -->
<?php get_template_part("/template-parts/contact_bottom"); ?>
<!-- contact bottom -->


<?php get_footer(); ?>
