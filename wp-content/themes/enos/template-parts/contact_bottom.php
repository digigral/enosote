
<section style="padding-top: 70px; padding-bottom: 70px; background-color: #f9f9f9;" class="extForm  mbr-parallax-background" id="extForm14-1e">

    <div class="container ">


        
        <h2  style="margin-bottom: 70px;" class="mbr-fonts-style align-center">
            <?php echo get_field("naslov", "option"); ?></h2>
        
        
        <div class="media-container-row">


            <div style=" padding-right: 15px;" class="col-md-10 col-lg-6">
                <div class="google-map" style="    width: 100%;
    height: 25rem;">
                    <iframe frameborder="0" style="border:0; width: inherit; height: 100%;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2749.8757582947956!2d14.0627768155901!3d46.43134207912424!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x477a9a99e7f4b313%3A0xc7831d73b9ef0e7f!2sENOS%2C%20oskrba%20s%20toplo%20vodo%2C%20paro%2C%20elektriko%20in%20plinom%2C%20d.d.!5e0!3m2!1sen!2ssi!4v1602224261622!5m2!1sen!2ssi" allowfullscreen=""></iframe>
                </div>
                <div class="row info-row justify-content-center pt-3">
                    <div class="first-column col-12 mbr-fonts-style display-7">
                        <ol>
                            <li>
                                <span class="info-title mbr-fonts-style">
                                    <?php echo get_field("levo1", "option"); ?>
                                </span>
                                <span class="info-value mbr-fonts-style">&nbsp;
                                  <?php echo get_field("levo2", "option"); ?></span>
                            </li>
                            <li>
                                <span class="info-title mbr-fonts-style">
                                  <?php echo get_field("levo3", "option"); ?>
                                </span>
                                <span class="info-value mbr-fonts-style">
                                  <?php echo get_field("desno1", "option"); ?></span>
                            </li>
                            <li>
                                <span class="info-title mbr-fonts-style">
                                  <?php echo get_field("desno2", "option"); ?>
                                </span>
                                <span class="info-value mbr-fonts-style">
                                    <?php echo get_field("desno3", "option"); ?>
                                  </span>
                            </li>
                        </ol>
                    </div>
                    
                </div>
            </div>
            
            <div    style=" padding-left: 15px;" class="col-md-10 col-lg-6 form-wrapper">            
                
                <h2 class="mbr-section-title form-title mbr-fonts-style display-5 pb-3"><?php echo get_field("naslov_-_kontakt", "option"); ?></h2>
                <p class="mbr-text form-text mbr-fonts-style m-0 pb-4 mbr-lighter display-7">
                    <?php echo get_field("naslov_-_kontakt_copy", "option"); ?>
                </p>
                <div class="form1 my-contact" data-form-type="formoid">
            
            
                      <?php echo do_shortcode('[contact-form-7 id="50" title="Contact form 1"]'); ?>
                    
                    
                </div>
            </div>
            
            

        </div>
   </div>
</section>