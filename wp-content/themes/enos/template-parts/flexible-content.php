<?php
if (have_rows('vsebina')) : ?>

    <?php while (have_rows('vsebina')) : the_row(); ?>

        <!-- prva -->
        <?php if (get_row_layout() == 'header') : ?>
            <?php get_template_part('template-parts/header_image'); ?>
        <?php elseif (get_row_layout() == 'sekcija_-_pomoc') : ?>
            <?php
            $naslov = get_sub_field('naslov');
            $boxi = get_sub_field('boxi');
            //d($boxi);
            ?>

            <section class="extFeatures cid-rTqbFsu8Bs" id="extFeatures8-34">

                <div class="container">
                    <h2 style="padding-bottom: 22px;" class="mbr-fonts-style mbr-section-title align-center display-2"><?php echo $naslov; ?></h2>

                    <div class="row justify-content-center pt-4">

                        <?php if ($boxi) : ?>
                            <?php foreach ($boxi as $b) : ?>

                                <div class="col-md-6 col-lg-3 row-item">
                                    <a style="color: black;" href="<?php echo $b['povezava']; ?>" <?php if ($b['newtab']) {
                                                                                                        echo 'target="_blank"';
                                                                                                    } ?>>
                                        <div class="wrapper">
                                            <div class="card-img pb-3 align-center">
                                                <span class="<?php echo $b['ikona']; ?>"></span>
                                            </div>
                                            <h4 class="mbr-fonts-style mbr-card-title align-center display-7"><?php echo $b['tekst']; ?></h4>
                                        </div>
                                    </a>
                                </div>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </section>
            <!-- prva -->
            <!-- _obvestilo -->
        <?php elseif (get_row_layout() == 'sekcija_-_obvestilo') : ?>
            <?php
            $ID = get_sub_field('id');
            $naslov = get_sub_field('naslov');
            $tekst = get_sub_field('tekst');
            $slika = get_sub_field('slika');
            $akordion = get_sub_field('akordion');
            //d($akordion);
            ?>
            <section class="toggle2 cid-rRw93LZ2KJ" id="toggle2-2w">
                <div class="offset-id" id="<?php echo $ID; ?>"></div>
                <div class="container">
                    <div class="media-container-row">
                        <div class="toggle-content">
                            <h2 class="mbr-section-title pb-3 align-left mbr-fonts-style display-2">
                                <?php echo $naslov; ?>
                            </h2>
                            <h3 class="mbr-section-subtitle align-left mbr-fonts-style display-7">
                                <?php echo $tekst; ?>
                            </h3>

                            <?php if ($akordion) : ?>

                                <div style="padding-top: 1rem!important;" id="bootstrap-toggle" class="toggle-panel accordionStyles tab-content pt-5 mt-2">

                                    <?php foreach ($akordion as $a) : ?>
                                        <div class="card">
                                            <div class="card-header" role="tab" id="headingOne">
                                                <a role="button" class="collapsed panel-title text-black" data-toggle="collapse" data-core="" href="#cc-<?php echo $a['id']; ?>" aria-expanded="false" aria-controls="collapse1">
                                                    <h4 class="mbr-fonts-style display-7">
                                                        <span class="sign mbr-iconfont mbri-arrow-down inactive"></span>
                                                        <?php echo $a['naslov']; ?>
                                                    </h4>
                                                </a>
                                            </div>
                                            <div id="cc-<?php echo $a['id']; ?>" class="panel-collapse noScroll collapse" role="tabpanel" aria-labelledby="headingOne">
                                                <div class="panel-body p-4">
                                                    <p class="mbr-fonts-style panel-text display-7">
                                                        <?php echo $a['vsebina']; ?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>

                            <?php endif; ?>


                        </div>
                        <div class="mbr-figure" style="width: 37%;">
                            <img src="<?php echo $slika; ?>" alt="" title="">
                        </div>
                    </div>
                </div>
            </section>


            <!-- _obvestilo -->

            <!-- _ugodnosti -->
        <?php elseif (get_row_layout() == 'sekcija_-_ugodnosti') : ?>
            <?php
            $ID = get_sub_field('id');
            $naslov = get_sub_field('naslov');
            $tekst = get_sub_field('tekst');

            $p1_naslov = get_sub_field('paket_1_-_naslov');
            $p1_tekst = get_sub_field('paket_1_teskt');
            $p1_slika = get_sub_field('paket_1_slika');

            $p2_naslov = get_sub_field('paket_2_-_naslov');
            $p2_tekst = get_sub_field('paket_2_teskt_copy');
            $p2_slika = get_sub_field('paket_2_slika');
            ?>

            <section class="extFeatures cid-rRwjNugQSQ" id="extFeatures28-32">
                <div class="offset-id" id="<?php echo $ID; ?>"></div>
                <div class="container">
                    <div class="media-container-row">
                        <div class="card col-12 col-md-4">
                            <h2 class="mbr-fonts-style mb-4 mbr-section-title display-2"><?php echo $naslov; ?></h2>
                            <h3 class="mbr-text section-text mbr-fonts-style mbr-light display-7"><?php echo $tekst; ?></h3>

                        </div>
                        <div class="card col-12 col-md-4">
                            <div class="card-img">
                                <img src="<?php echo $p1_slika; ?>" alt="" title="">
                            </div>
                            <div class="card-box">
                                <h4 class="card-title mbr-fonts-style display-5">
                                    <?php echo $p1_naslov; ?>
                                </h4>
                                <div class="mbr-text card-text mbr-fonts-style mbr-light display-7">
                                    <?php echo $p1_tekst; ?>
                                </div>
                                <div style="text-align: center">
                                    <a class="btn btn-sm btn-primary display-4 " id="plin1"> POVPRAŠEVANJE</a>
                                </div>

                            </div>
                        </div>
                        <div class="card col-12 col-md-4">
                            <div class="card-img">
                                <img src="<?php echo $p2_slika; ?>" alt="" title="">
                            </div>
                            <div class="card-box">
                                <h4 class="card-title mbr-fonts-style display-5">
                                    <?php echo $p2_naslov; ?>
                                </h4>
                                <div class="mbr-text card-text mbr-fonts-style mbr-light display-7">
                                    <?php echo $p2_tekst; ?>
                                </div>
                                <div style="text-align: center">
                                    <a class="btn btn-sm btn-primary display-4 " id="plin2"> POVPRAŠEVANJE</a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!-- _ugodnosti -->


            <!-- _levo_tekst_desno_slika -->
        <?php elseif (get_row_layout() == 'sekcija_-_levo_tekst_desno_slika') : ?>
            <?php
            $ID = get_sub_field('id');
            $naslov = get_sub_field('naslov');
            $tekst = get_sub_field('tekst');
            $gumb_tekst = get_sub_field('gumb_tekst');
            $gumb_povezava = get_sub_field('gumb_povezava');
            $slika_desno = get_sub_field('slika_desno');
            $no_background = get_sub_field('no_background');
            $new_tab = get_sub_field('povezava_nov_tab');
            ?>
            <section class="extHeader cid-rRw9Xe57pW <?php if ($no_background) {
                                                            echo 'no-background';
                                                        } ?>" id="extHeader14-2z">
                <div class="offset-id" id="<?php echo $ID; ?>"></div>
                <div class="container">
                    <div class="row main-row">
                        <div class="text-content mb-4 col-lg-6">
                            <h2><?php echo $naslov; ?></h2>
                            <div class="list counter-container col-12  mbr-fonts-style mbr-black display-7">
                                <?php echo $tekst; ?>
                            </div>
                            <?php if ($gumb_povezava) : ?>
                                <div class="mbr-section-btn align-left"><a class="btn btn-md btn-primary display-4" href="<?php echo $gumb_povezava; ?>" <?php if ($new_tab) {
                                                                                                                                                                echo 'target="_blank"';
                                                                                                                                                            } ?>><?php echo $gumb_tekst; ?></a>
                                </div>
                            <?php endif; ?>
                        </div>
                        <div class="align-self-center col-lg-6">
                            <img src="<?php echo $slika_desno; ?>" alt="Mobirise" title="">
                        </div>
                    </div>
                </div>
            </section>
            <!-- _levo_tekst_desno_slika -->

        <?php elseif (get_row_layout() == 'sekcija_-_desno_tekst_levo_slika') : ?>
            <?php
            $ID = get_sub_field('id');
            $naslov = get_sub_field('naslov');
            $tekst = get_sub_field('tekst');
            $gumb_tekst = get_sub_field('gumb_tekst');
            $gumb_povezava = get_sub_field('gumb_povezava');
            $new_tab = get_sub_field('povezava_nov_tab');
            $slika_desno = get_sub_field('slika_desno');
            $no_background = get_sub_field('no_background');
            ?>
            <section class="extHeader cid-rRw9Xe57pW <?php if ($no_background) {
                                                            echo 'no-background';
                                                        } ?>" id="extHeader14-2z">
                <div class="offset-id" id="<?php echo $ID; ?>"></div>
                <div class="container">
                    <div class="row main-row">
                        <div class="align-self-center col-lg-6">
                            <img src="<?php echo $slika_desno; ?>" alt="Mobirise" title="">
                        </div>
                        <div class="text-content mb-4 col-lg-6">
                            <h2><?php echo $naslov; ?></h2>
                            <div class="list counter-container col-12  mbr-fonts-style mbr-black display-7">
                                <?php echo $tekst; ?>
                            </div>
                            <?php if ($gumb_povezava) : ?>
                                <div class="mbr-section-btn align-left"><a class="btn btn-md btn-primary display-4" href="<?php echo $gumb_povezava; ?>" <?php if ($new_tab) {
                                                                                                                                                                echo 'target="_blank"';
                                                                                                                                                            } ?>><?php echo $gumb_tekst; ?></a>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </section>

            <!-- _levo_tekst_desno_slika -->

            <!-- _ostale_oblike -->
        <?php elseif (get_row_layout() == 'sekcija_-_ostale_oblike') : ?>
            <?php
            $ID = get_sub_field('id');
            $naslov = get_sub_field('naslov');
            $gumb1_t = get_sub_field('gumb_tekst');
            $gumb1_link = get_sub_field('gumb_povezava');
            $gumb2_t = get_sub_field('gumb_tekst_2');
            $gumb2_link = get_sub_field('gumb_povezava_2');
            $tekst = get_sub_field('tekst');
            ?>

            <section class="extTabs cid-rRwbho5ruL mbr-parallax-background" id="extTabs6-30">
                <div class="mbr-overlay">
                </div>
                <div class="offset-id" id="<?php echo $ID; ?>"></div>
                <div class="container">
                    <h2 class="mbr-section-title align-center pb-5 mbr-fonts-style display-2">
                        <?php echo $naslov; ?>
                    </h2>
                    <div class="media-container-row">
                        <div class="col-12 col-md-10">
                            <ul class="nav nav-tabs" role="tablist">
                                <li style="margin-right: 10px;margin-left: 10px;" class="nav-item">
                                    <a class="nav-link mbr-fonts-style show active display-7" href="<?php echo $gumb1_link; ?>" aria-selected="true">
                                        <?php echo $gumb1_t; ?>
                                    </a></li>
                                <li class="nav-item">
                                    <a class="nav-link mbr-fonts-style show active display-7" href="<?php echo $gumb2_link; ?>" aria-selected="true">
                                        <?php echo $gumb2_t; ?>
                                    </a></li>
                            </ul>

                        </div>
                    </div>
                    <?php echo $tekst; ?>


                </div>
            </section>

            <!-- _ostale_oblike -->

            <!-- _ostale_oblike -->
        <?php elseif (get_row_layout() == 'sekcija_html') : ?>
            <?php
            $ID = get_sub_field('id');
            $tekst = get_sub_field('text');
            ?>

            <section class="extTabs cid-rRw9Xe57pW no-background">
                <div class="offset-id" id="<?php echo $ID; ?>"></div>
                <div class="container">
                    <?php echo $tekst; ?>
                </div>
            </section>

            <!-- _ostale_oblike -->
        <?php elseif (get_row_layout() == 'elementi') : ?>
            <?php
            $boxes = get_field("elementi_r"); ?>

            <?php if ($boxes) : ?>
                <?php foreach ($boxes as $b) : ?>
                    <div class="card col-12 col-md-6 p-3 col-lg-4">
                        <div class="card-wrapper">
                            <div class="card-img">
                                <img src="<?php echo $b['slika']; ?>" alt="" title="">
                            </div>
                            <div class="card-box">
                                <h4 class="card-title mbr-fonts-style display-5"><?php echo $b['title']; ?></h4>
                                <p class="mbr-text mbr-fonts-style display-7"><?php echo $b['tekst']; ?></p>
                                <!--Btn-->
                                <div class="mbr-section-btn align-left"><a href="<?php echo $b['povezava']; ?>" class="btn btn-warning-outline display-4">Preverite</a></div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>

            <!-- zadnje_3_novice -->
        <?php elseif (get_row_layout() == 'zadnje_3_novice') : ?>

            <section style="padding-bottom: 40px;" class="extFeatures cid-rRlyP1cqF3" id="extFeatures29-1d">

                <?php
                $naslov = get_sub_field('naslov');
                ?>

                <?php
                // get 3 latest news
                $args = array(
                    'post_type' => 'novice',
                    'posts_per_page' => 3,
                );

                $novice = new WP_Query($args);
                //d($novice->posts);
                ?>


                <div class="container">

                    <h2 class="mbr-fonts-style mb-4 align-center display-2"><?php echo $naslov; ?></h2>

                    <div class="media-container-row">


                        <?php if ($novice) : ?>
                            <?php foreach ($novice->posts as $n) : ?>
                                <div class="card col-12 col-md-4 col-lg-4">
                                    <div class="card-img">
                                        <img src="<?php echo get_the_post_thumbnail_url($n->ID); ?>" alt="" title="">
                                    </div>
                                    <div class="card-box">
                                        <p class="date mb-4">
                                            <span><?php echo get_the_date("d.m.j", $n->ID); ?></span>
                                        </p>
                                        <h4 class="card-title mbr-fonts-style display-5">
                                            <?php echo $n->post_title; ?>
                                        </h4>
                                        <p class="mbr-text mbr-fonts-style display-7">
                                            <?php echo get_the_excerpt($n->ID); ?>
                                        </p>
                                        <div class="mbr-section-btn"><a class="btn-underline mr-3 display-7" href="<?php echo get_permalink($n->ID); ?>">Preverite
                                                &gt;</a></div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        <?php endif; ?>

                    </div>


                </div>
            </section>

            <!-- zadnje_3_novice -->
            <!-- 4_barvni_kvadrati -->
        <?php elseif (get_row_layout() == '4_barvni_kvadrati') : ?>


            <?php
            $naslov = get_sub_field('naslov');
            $podnaslov = get_sub_field('podnaslov');
            $kvadrati = get_sub_field('kvadrati');
            // /d($kvadrati);
            ?>

            <section style="    padding-top: 20px;     padding-bottom: 75px;" class="extFeatures cid-rRlwHkrH56" id="extFeatures13-1b">

                <style>
                    .cid-rRlwHkrH56 .wrapper {
                        height: 250px;
                    }


                    .cid-rRlwHkrH56 .wrapper .mbr-text {
                        height: 210px;
                        font-size: 20px;
                    }
                </style>


                <div class="container">
                    <h2 class="mbr-fonts-style mbr-section-title align-center display-2">
                        <?php echo $naslov; ?>
                    </h2>
                    <h3 style="    margin-bottom: 1.7rem;   color: #8d97ad;   margin-top: 0.5rem; " class="mbr-fonts-style mbr-section-subtitle align-center mbr-light pt-3 display-5">
                        <?php echo $podnaslov; ?>
                    </h3>

                    <div class="media-container-row">
                        <?php
                        if ($kvadrati) :
                            foreach ($kvadrati as $key => $item) :
                        ?>
                                <div class="col-md-6 row-item col-lg-3">
                                    <a href="<?php echo $item['gumb_povezava']; ?>">
                                        <div class="wrapper card<?php echo $key + 1; ?>">
                                            <h4 class="mbr-fonts-style mbr-card-title align-left mbr-white display-5"><?php echo $item['naslov']; ?></h4>
                                            <p class="mbr-text mbr-fonts-style align-left display-7"><?php echo $item['tekst']; ?></p>
                                        </div>
                                    </a>
                                </div>
                        <?php
                            endforeach;
                        endif;
                        ?>

                    </div>
                </div>
            </section>
            <!-- 4_barvni_kvadrati -->
        <?php elseif (get_row_layout() == 'card_grid') : ?>

            <?php
            $osnovni_blok_storitve = get_sub_field("block_rep");

            //d($osnovni_blok_storitve[0]);
            if ($osnovni_blok_storitve) :
            ?>
                <section style="    margin-top: 0.2rem; padding-top: 50px; border-top: 1px solid #e0e0e0;" class="extFeatures cid-rRvYiozT0l" id="extFeatures40-25">
                    <div class="container">
                        <div class="rows-wrapper">
                            <div class="row main align-items-center justify-content-center">

                                <?php foreach ($osnovni_blok_storitve as $block) : ?>
                                    <!-- first -->
                                    <div class="card col-md-3">
                                        <div class="text-element d-flex align-items-center order-md-2">
                                            <div class="text-content align-left">
                                                <div class="card-img pb-3">

                                                </div>
                                                <h2 class="mbr-title  mbr-fonts-style display-5">
                                                    <?php echo $block['naslov']; ?></h2>
                                                <p class="mbr-text card-text mbr-light mbr-fonts-style display-7">
                                                    <?php echo $block['tekst']; ?>
                                                </p>
                                                <div class="mbr-section-btn"><a class="btn-underline mr-3 text-info display-7" href="<?php echo $block['povezava']; ?>">Preveri</a></div>
                                            </div>
                                        </div>
                                        <div class="image-element">
                                            <div class="img-wrap">
                                                <img src="<?php echo $block['slika']; ?>" alt="" title="">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- first -->
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </section>
            <?php endif; ?>

        <?php elseif (get_row_layout() == 'z_naslovom_levo_tekst_-_desno_slika') : ?>

            <?php
            $naslov = get_sub_field('naslov');
            $podnaslov = get_sub_field('podnaslov');
            $id =  get_sub_field('id_sekcije');
            $text = get_sub_field('tekst');
            $image  = get_sub_field('slika');
            ?>

            <!-- on storitev -->

            <section style="padding-top: 40px; padding-bottom: 30px; background-color: #f9f9f9;" class="mbr-section content4 cid-rRvZ7p1VQH" id="content4-27">
                <div class="offset-id" id="<?php echo $id;  ?>"></div>
                <div class="container">
                    <div class="media-container-row">
                        <div class="title col-12 col-md-8">
                            <h2 class="align-center pb-3 mbr-fonts-style display-2">
                                <?php echo $naslov; ?></h2>
                            <h3 class="mbr-section-subtitle align-center mbr-light mbr-fonts-style display-5">
                                <?php echo $podnaslov; ?>
                            </h3>

                        </div>
                    </div>
                </div>
            </section>
            <section class="mbr-section content7 cid-rRvYZytI8K" id="content7-26">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-md-6 order-2 order-md-1">
                            <div class="mbr-section-text">
                                <p class="mbr-text align-right mb-0 mbr-fonts-style display-7">
                                    <?php echo $text; ?>
                                </p>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 order-1 order-md-2 ">
                            <div class="mbr-figure" style="width: 100%;">
                                <img src="<?php echo $image; ?>" alt="" title="">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- on storitev -->
        <?php elseif (get_row_layout() == 'z_naslovom_desno_tekst_-_levo_slika') : ?>
            <?php
            $naslov = get_sub_field('naslov');
            $podnaslov = get_sub_field('podnaslov');
            $id =  get_sub_field('id_sekcije');
            $text = get_sub_field('tekst');
            $image  = get_sub_field('slika');
            ?>
            <!-- on storitev -->
            <section style="padding-top: 40px; padding-bottom: 30px; background-color: #f9f9f9;" class="mbr-section content4 cid-rRvZ7p1VQH" id="content4-27">
                <div class="offset-id" id="<?php echo $id;  ?>"></div>
                <div class="container">
                    <div class="media-container-row">
                        <div class="title col-12 col-md-8">
                            <h2 class="align-center pb-3 mbr-fonts-style display-2">
                                <?php echo $naslov; ?></h2>
                            <h3 class="mbr-section-subtitle align-center mbr-light mbr-fonts-style display-5">
                                <?php echo $podnaslov; ?>
                            </h3>

                        </div>
                    </div>
                </div>
            </section>
            <section class="mbr-section content7 cid-rRvYZytI8K" id="content7-26">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-md-6 order-2 order-md-1">
                            <div class="mbr-figure" style="width: 100%;">
                                <img src="<?php echo $image; ?>" alt="" title="">
                            </div>
                        </div>
                        <div class="col-12 col-md-6 order-1 order-md-2 ">
                            <div class="mbr-section-text">
                                <p class="mbr-text align-right mb-0 mbr-fonts-style display-7">
                                    <?php echo $text; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- on storitev -->
        <?php elseif (get_row_layout() == 'maps_lng') : ?>
            <div id="gmaps"></div>

        <?php elseif (get_row_layout() == 'obvestila') : ?>
            <section style=" padding-top: 80px;    padding-bottom: 90px;" class="toggle2 cid-rRffn5xTMV" id="toggle2-h">
                <div class="container">
                    <div class="media-container-row">
                        <div class="toggle-content">
                            <h2 class="mbr-section-title pb-3 align-left mbr-fonts-style display-2">
                                <?php echo get_sub_field('obvestila_za_odjemalce_naslov'); ?></h2>
                            <h3 class="mbr-section-subtitle align-left mbr-fonts-style display-7"><?php echo get_sub_field('obvestila_za_odjemalce_podtext'); ?></h3>
                            <div id="bootstrap-toggle" class="toggle-panel accordionStyles tab-content pt-5 mt-2">
                                <?php $obvestila = get_sub_field('obvestila_za_odjemalce_obvestila');
                                if ($obvestila) :
                                    foreach ($obvestila as $key => $obvestilo) :
                                ?>
                                        <div class="card">
                                            <div class="card-header" role="tab" id="heading<?php echo $key; ?>">
                                                <a role="button" class="collapsed panel-title text-black" data-toggle="collapse" data-core="" href="#collapse<?php echo $key; ?>_13" aria-expanded="false" aria-controls="collapse1">
                                                    <h4 class="mbr-fonts-style display-7">
                                                        <span class="sign mbr-iconfont mbri-arrow-down inactive"></span>
                                                        <?php echo $obvestilo['obvestlo_naslov']; ?>
                                                    </h4>
                                                </a>
                                            </div>
                                            <div id="collapse<?php echo $key; ?>_13" class="panel-collapse noScroll collapse" role="tabpanel" aria-labelledby="heading<?php echo $key; ?>">
                                                <div class="panel-body p-4">
                                                    <p class="mbr-fonts-style panel-text display-7">
                                                        <?php echo $obvestilo['obvestilo_text']; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                <?php endforeach;
                                endif;
                                ?>
                            </div>
                        </div>
                        <div class="mbr-figure" style="width: 37%;">
                            <img src="/wp-content/themes/enos/img/ote-obvestila.jpg" alt="enosote" title="">
                        </div>
                    </div>
                </div>
            </section>


        <?php endif; ?>
    <?php endwhile; ?>
<?php endif; ?>