
<!-- uvodna sekcija -->
<?php
$ozadje = get_sub_field("ozadje_header");
?>
<section style="padding-top: 90px; background: url(<?php echo $ozadje['url']; ?>)" class="mbr-section content5 cid-rRlhW5ZFoK mbr-parallax-background" id="content5-13">
    <div class="mbr-overlay">
    </div>

    <div class="container">
        <div class="media-container-row">
            <div class="title col-12 col-md-8">
                <h2 class="align-center mbr-bold mbr-white pb-3 mbr-fonts-style display-1">
                    <?php echo get_sub_field("naslov_header"); ?>
                </h2>
                <h3 class="mbr-section-subtitle align-center mbr-light mbr-white pb-3 mbr-fonts-style display-5">
                    <?php echo get_sub_field("podnaslov_header"); ?>
                </h3>
            </div>
        </div>
    </div>
</section>
<!-- uvodna sekcija -->
