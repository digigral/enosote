<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
?>


<section class="" style="padding: 60px 0;background-color: #f9f9f9;">
    <div class="container">
        <h1>Novice</h1>
    </div>
</section>




<section style="padding-top: 10px; padding-bottom: 40px;" class="extFeatures cid-rRfeXMcLNy no-background" id="extFeatures29-e">

    <?php
    // get 3 latest news
    $args = array(
        'post_type'        => 'novice',
        'posts_per_page'   => -1,
    );

    $novice = new WP_Query( $args );
    //d($novice->posts);
    ?>

<?php
if (have_rows('vsebina',1328)) : ?>
    <?php while (have_rows('vsebina',1328)) : the_row(); ?>
    <?php if (get_row_layout() == 'obvestila') : ?>
            <section style=" padding-top: 80px;    padding-bottom: 90px;" class="toggle2 cid-rRffn5xTMV" id="toggle2-h">
                <div class="container">
                    <div class="media-container-row">
                        <div class="toggle-content">
                            <h2 class="mbr-section-title pb-3 align-left mbr-fonts-style display-2">
                                <?php echo get_sub_field('obvestila_za_odjemalce_naslov'); ?></h2>
                            <h3 class="mbr-section-subtitle align-left mbr-fonts-style display-7"><?php echo get_sub_field('obvestila_za_odjemalce_podtext'); ?></h3>
                            <div id="bootstrap-toggle" class="toggle-panel accordionStyles tab-content pt-5 mt-2">
                                <?php $obvestila = get_sub_field('obvestila_za_odjemalce_obvestila');
                                if ($obvestila) :
                                    foreach ($obvestila as $key => $obvestilo) :
                                ?>
                                        <div class="card">
                                            <div class="card-header" role="tab" id="heading<?php echo $key; ?>">
                                                <a role="button" class="collapsed panel-title text-black" data-toggle="collapse" data-core="" href="#collapse<?php echo $key; ?>_13" aria-expanded="false" aria-controls="collapse1">
                                                    <h4 class="mbr-fonts-style display-7">
                                                        <span class="sign mbr-iconfont mbri-arrow-down inactive"></span>
                                                        <?php echo $obvestilo['obvestlo_naslov']; ?>
                                                    </h4>
                                                </a>
                                            </div>
                                            <div id="collapse<?php echo $key; ?>_13" class="panel-collapse noScroll collapse" role="tabpanel" aria-labelledby="heading<?php echo $key; ?>">
                                                <div class="panel-body p-4">
                                                    <p class="mbr-fonts-style panel-text display-7">
                                                        <?php echo $obvestilo['obvestilo_text']; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                <?php endforeach;
                                endif;
                                ?>
                            </div>
                        </div>
                        <div class="mbr-figure" style="width: 37%;">
                            <img src="/wp-content/themes/enos/img/ote-obvestila.jpg" alt="enosote" title="">
                        </div>
                    </div>
                </div>
            </section>


        <?php endif; ?>
    <?php endwhile; ?>
<?php endif; ?>

    <div class="container">
        <div class="container">
            <h2 class="mbr-fonts-style mb-4 align-center display-2"><?php echo get_field("novice_naslov"); ?></h2>

            <div class="row">

                <?php if($novice): ?>
                    <?php foreach($novice->posts as $n): ?>
                            <div class="col-12">
                                <a style="position: absolute; top:0; left:0; height: 100%; width: 100%; z-index: 99" href="<?php echo get_permalink( $n->ID ); ?>" > </a>
                                <div class="row">
                                    <div class="col-12 col-md-6">
                                            <img style="width: 100%; height: auto" src="<?php echo get_the_post_thumbnail_url( $n->ID , 'full'); ?>" alt="" title="">
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <div>
                                            <p class="date mb-4">
                                                <span><?php echo get_the_date( "d.m.Y", $n->ID ); ?></span>
                                            </p>
                                            <h4 class="mbr-fonts-style display-5">
                                                <?php echo $n->post_title; ?>
                                            </h4>
                                            <p class="mbr-text mbr-fonts-style display-7">
                                                <?php echo get_the_excerpt( $n->ID ); ?>
                                            </p>
                                            <div class="mbr-section-btn"><a class="btn-underline mr-3 display-7" href="<?php echo get_permalink( $n->ID ); ?>" >Preverite &gt;</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    <?php endforeach; ?>
                <?php endif; ?>



            </div>


        </div>
    </div>
</section>

		

<!-- contact bottom -->
<?php get_template_part("/template-parts/contact_bottom"); ?>
<!-- contact bottom -->


<?php get_footer(); ?>
