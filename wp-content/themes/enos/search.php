<?php
/**
 * The template for displaying search results pages.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();


$container = get_theme_mod( 'understrap_container_type' );

?>
	<!-- subpages hero -->
	<section style="background: url(http://fersped.digiapps.my/wp-content/uploads/2019/09/bg-kontakt-1.jpg);"  class="section-hero-subpages">
			<div class="hero-center-subpages">

				<h1><?php the_title(); ?></h1>
				<p>
					<?php echo get_field("page_subtitle");  ?>
				</p>

			</div>
	</section>
	<!-- /subpages hero -->

	<div class="container" id="content">
		<div class="row">

			<div class="col-md-4">
					<?php get_template_part( '/sidebar-templates/sidebar-company' ); ?>
			</div>

			<div class="col-md-8 content-area" id="primary">
				<div class="wrapper" id="search-wrapper">
					<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

							<!-- Do the left sidebar check and opens the primary div -->
							<?php get_template_part( 'global-templates/left-sidebar-check' ); ?>

							<main class="site-main" id="main">

								<?php if ( have_posts() ) : ?>

									<header class="page-header">

											<h1 class="page-title">
												<?php
												printf(
													/* translators: %s: query term */
													esc_html__( 'Search Results for: %s', 'understrap' ),
													'<span>' . get_search_query() . '</span>'
												);
												?>
											</h1>

									</header><!-- .page-header -->

									<?php /* Start the Loop */ ?>
									<?php while ( have_posts() ) : the_post(); ?>

										<?php
										/**
										 * Run the loop for the search to output the results.
										 * If you want to overload this in a child theme then include a file
										 * called content-search.php and that will be used instead.
										 */
										get_template_part( 'loop-templates/content', 'search' );
										?>

									<?php endwhile; ?>

								<?php else : ?>

									<?php get_template_part( 'loop-templates/content', 'none' ); ?>

								<?php endif; ?>

							</main><!-- #main -->

					</div><!-- #content -->
				</div><!-- #search-wrapper -->
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>
