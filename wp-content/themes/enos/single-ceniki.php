<?php
/**
 * The template for displaying all single posts.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<div class="cenik container pt-5 pb-5">
    <div class="row pb-3">
        <div class="col-12">
            <?php $terms = get_the_terms($post->ID,'leto'); ?>
            <a href="/leto/<?php echo $terms[0]->slug ?>" > > Nazaj na arhiv</a>
        </div>
    </div>

    <h2><?php echo get_the_title(); ?></h2>
    <div class="vsebina">
        <div class="vsebina-zgoraj">
            <?php echo get_field('vsebina_zgoraj') ?>
        </div>

        <?php
        $tabela_naslov  = get_field('naslovi_tabel');   //d($tabela_naslov);
        $tabela_vsebina  = get_field('vsebina_tabele'); //d($tabela_vsebina);
        ?>

        <!-- cenik template -->
        <section style="<?php if( $cenik_conunter == 0 ): ?> padding-top: 30px;  padding-bottom: 45px; <?php else: ?> padding-top: 0px;  padding-bottom: 60px; <?php endif; ?>" class="section-table cid-rTqk06IQgf" id="table1-3f">
            <div class="container container-table">
                <div class="table-wrapper">
                    <div class="container scroll">
                        <table class="table" cellspacing="0">
                            <thead>
                            <tr class="table-heads ">
                                <?php if($tabela_naslov):  ?>
                                    <?php foreach($tabela_naslov as $n): ?>
                                        <th class="head-item mbr-fonts-style display-7">
                                            <?php echo $n['tekst']; ?>
                                        </th>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </tr>
                            </thead>
                            <?php if($tabela_vsebina):  ?>
                                <tbody>
                                <?php foreach($tabela_vsebina as $t):  ?>
                                    <tr>
                                        <?php foreach(  $t['vrstice'] as $t1  ): ?>
                                            <td class="body-item mbr-fonts-style display-7"><?php echo $t1['tekst']; ?></td>
                                        <?php endforeach; ?>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            <?php endif; ?>
                        </table>
                    </div>
                </div>
            </div>
        </section>
        <?php $cenik_conunter = $cenik_conunter + 1; ?>
        <div class="vsebina-spodaj">
            <?php echo get_field('vsebina_spodaj') ?>
        </div>
    </div>
</div>


<!-- contact bottom -->
<?php get_template_part("/template-parts/contact_bottom"); ?>
<!-- contact bottom -->


<?php get_footer(); ?>
