<?php
/**
 * The header for our theme.
 *
 * Displays all of the
 *
 * @package understrap
 */

// Exit if accessed directly.
defined('ABSPATH') || exit;
?>
<!DOCTYPE html>
<html
    <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="profile" href="http://gmpg.org/xfn/11">


    <link rel="stylesheet" href="/wp-content/themes/enos/assets/web/assets/mobirise-icons-bold/mobirise-icons-bold.css">
    <link rel="stylesheet" href="/wp-content/themes/enos/assets/icons-mind/style.css">
    <link rel="stylesheet" href="/wp-content/themes/enos/assets/simple-line-icons/simple-line-icons.css">
    <link rel="stylesheet" href="/wp-content/themes/enos/assets/bootstrap-material-design-font/css/material.css">
    <link rel="stylesheet" href="/wp-content/themes/enos/assets/icon54/style.css">

    <link rel="stylesheet" href="/wp-content/themes/enos/assets/web/assets/mobirise-icons/mobirise-icons.css">

    <link rel="stylesheet" href="/wp-content/themes/enos/assets/font-awesome/css/font-awesome.css">

    <link rel="stylesheet" href="/wp-content/themes/enos/assets/web/assets/mobirise-icons2/mobirise2.css">

    <link rel="stylesheet" href="/wp-content/themes/enos/assets/tether/tether.min.css">
    <link rel="stylesheet" href="/wp-content/themes/enos/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/wp-content/themes/enos/assets/bootstrap/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="/wp-content/themes/enos/assets/bootstrap/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="/wp-content/themes/enos/assets/dropdown/css/style.css">
    <link rel="stylesheet" href="/wp-content/themes/enos/assets/socicon/css/styles.css">
    <link rel="stylesheet" href="/wp-content/themes/enos/assets/theme/css/style.css">
    <link rel="stylesheet" href="/wp-content/themes/enos/assets/mobirise/css/mbr-additional.css" type="text/css">

    <link rel="stylesheet" href="/wp-content/themes/enos/my.css" type="text/css">
    <link rel="icon" href="/wp-content/themes/enos/img/favicon.ico" sizes="16x16 32x32" type="image/png">
    <?php wp_head(); ?>

    <style>

        .navbar-short .menu-content-top {
            display: none !important;
        }

    </style>
		<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-167862902-2"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-167862902-2');
	</script>
</head>
<body>
    <section style="background: white;" class="extMenu2 menu cid-rReUTjH1zB" once="menu" id="extMenu8-3">

        <div style="max-width: 1400px;" class="container">
            <div class="row">
                <div class="col-md-12">
                    <nav class="navbar navbar-dropdown2 beta-menu align-items-center navbar-fixed-top navbar-toggleable-sm">
                        <div style="    padding-top: 16px; padding-bottom: 8px; display: block !important;"
                             class="menu-content-top order-1 order-lg-0" id="topLine">
                            <div class="row">
                                <div class="col-12 col-lg-3">
                                    <div style="" class="content-left-side">
                                        <p class="mbr-fonts-style content-text mbr-black display-4"><?php echo get_field('zgornji_levi_text', 'options') ?></p>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-9">
                                    <div style="margin-top: -4px;    padding-bottom: 1px;" class="content-right-side"
                                         data-app-modern-menu="true">
                                        <a class="content-link link mbr-black display-4"
                                           href="<?php echo get_field('naslov_link', 'options') ?>" target="_blank"  id="header-location">
                                            <span class="mobi-mbri mobi-mbri-map-pin mbr-iconfont mbr-iconfont-btn"></span>
                                            <?php echo get_field('naslov', 'options') ?>
                                        </a>
                                        <a class="content-link link mbr-black display-4" id="header-mail"
                                           href="mailto:<?php echo get_field('mail', 'options') ?>">
                                            <span class="mbri-letter mbr-iconfont mbr-iconfont-btn "></span>
                                            <?php echo get_field('mail', 'options') ?>
                                        </a>
                                        <a class="content-link link mbr-black display-4" id="header-call"
                                           href="tel:<?php echo get_field('telefon', 'options') ?>">
                                            <span class="mobi-mbri mobi-mbri-phone mbr-iconfont mbr-iconfont-btn"></span>
                                            <?php echo get_field('telefon', 'options') ?>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="menu-bottom order-0 order-lg-1">

                            <div class="menu-logo">
                                <div style="margin-left: 0rem;" class="navbar-brand">
                                                     <span class="navbar-logo">
                                                             <a href="/">
                                                                     <img style="height: 4rem;"
                                                                          src="/wp-content/themes/enos/img/enos_ote_logo.png"
                                                                          alt="Mobirise" title="">
                                                             </a>
                                                     </span>

                                </div>
                                <button class="navbar-toggler" type="button" data-toggle="collapse"
                                        data-target="#navbarSupportedContent, #topLine"
                                        aria-controls="navbarSupportedContent" aria-expanded="false"
                                        aria-label="Toggle navigation">
                                    <div class="hamburger">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </div>
                                </button>
                            </div>

                            <?php

                            $meni = get_field("prvi_nivo", "option");
                            //d($meni);
                            ?>


                            <div style="padding-right: 0rem;" class="collapse navbar-collapse" id="navbarSupportedContent">

                                <?php if ($meni): ?>
                                    <ul class="navbar-nav nav-dropdown js-float-line" data-app-modern-menu="true">

                                        <?php foreach ($meni as $key=>$m):

                                            ?>


                                            <?php if ($m['drugi_nivo'] != false): ?>

                                                <li class="nav-item dropdown k<?php echo $key; ?>">
                                         <span style="padding-left: 5px !important;"
                                               class="aa nav-link link mbr-black dropdown-toggle2 display-4"
                                               href="<?php echo $m['link']['url']; ?>" data-toggle="dropdown-submenu"
                                               aria-expanded="false" >


                                         <span class="<?php echo $m['icon']; ?>"></span>
                                         &nbsp;



                                     </span>


                                                <a class=" nav-link link mbr-black  display-4"
                                                   href="<?php echo $m['link']['url']; ?>" target="<?php echo $m['link']['target']; ?>">
                                                    <?php echo $m['text']; ?>
                                                </a>

                                                <?php if ($m['drugi_nivo']): ?>
                                                    <div class="dropdown-menu">
                                                        <?php foreach ($m['drugi_nivo'] as $drugi): ?>
                                                            <a class="mbr-black dropdown-item display-4"
                                                               href="<?php echo $drugi['link']['url']; ?>" target="<?php echo $drugi['link']['target']; ?>"><?php echo $drugi['text']; ?>
                                                                <br></a>
                                                        <?php endforeach; ?>
                                                    </div>
                                                <?php endif; ?>

                                            </li>

                                        <?php else: ?>

                                            <li class="nav-item">
                                                <a style="padding-left: 5px !important;"
                                                   class="nav-link link mbr-black display-4"
                                                   href="<?php echo $m['link']['url']; ?>" target="<?php echo $m['link']['target']; ?>">
                                                    <span class="<?php echo $m['icon']; ?>"></span>
                                                    &nbsp; <?php echo $m['text']; ?></a>
                                            </li>

                                        <?php endif; ?>

                                    <?php endforeach; ?>


                                </ul>
                            <?php endif; ?>

<!--                            <div class="navbar-buttons mbr-section-btn">-->
<!--                                <a style="margin-right: 0;" class="btn btn-sm btn-primary display-4 kontakt" href="#"><span-->
<!--                                            class="mbrib-contact-form mbr-iconfont mbr-iconfont-btn"></span>-->
<!--                                    KONTAKT</a>-->
<!--                            </div>-->
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</section>
<div class="wraper-all-pages">
