</div>
<section style="background: white;" class="extFooter cid-rReYpbCC2C" id="extFooter20-b">

  <style>
      .extFooter li:before {
  
      }
      .extFooter li {

      }
    
  </style>

    

    <div  class="container">
        <div class="row content text-white">
            <div class="col-12 col-lg-4 col-md-4   mbr-fonts-style mbr-black">
                <div class="media-wrap">
                    <a href="/">
                        <img style="height: 4rem !important; width: auto;" src="<?php echo get_field('logo','options'); ?>" alt="" title="">
                    </a>
                </div>
                <p style="color: black;" class="mbr-fonts-style mbr-black logo-subtitle display-7">
                    <?php echo get_field('tekst_o_nas_footer','options'); ?>
                    </p>
            </div>
            <div class="col-12 col-lg-4 col-md-4  mbr-fonts-style mbr-black">
                <h5 class="pb-3 column-title display-5">
                    <?php echo get_field('naslov_2_stolpec','options'); ?></h5>
                <div class="contact-list display-7">
                    
                    
                <?php echo get_field('tekst_2_stolpec','options'); ?>
                  
                  </div>
            </div>
            
            <style>
            
            .footer-link:hover {
              color: #bed630 !important;
            }
            
            </style>
            


            
            <div class="col-12 col-lg-4 col-md-4 mbr-fonts-style mbr-black">
                <h5 class="pb-3 column-title display-5">
                    <?php echo get_field('naslov_4_stolpec','options'); ?></h5>
                <div class="mbr-text mbr-fonts-style display-7">
                  
                  <?php $cetrti = get_field("povezave_4_stolpec","options"); ?>
                  
                  <?php if($cetrti): ?>
                  <ul class="list">
                      <?php foreach($cetrti as $t): ?>
                      <li><a class="footer-link" style="color: black; " href="<?php echo $t['link']; ?>"><?php echo $t['tekst']; ?></a></li>
                    <?php endforeach; ?>
                  </ul>
                  <?php endif; ?>
                  
                </div>
            </div>
            
        </div>



  
        
        
    </div>
</section>


<section style="background: black; padding-top: 15px;     padding-bottom: 3px;" class="extFooter cid-rReYpbCC2C" id="extFooter20-b">

  <style>
      .extFooter li:before {
        display: none !important;
      }
      .extFooter li {
          list-style: disc !important;
      }
    
  </style>

    

    <div style="max-width: 1400px;" class="container">
        <div class="row content text-white">
          
            <div class="col-12 col-lg-9 col-md-9">
              <p style="color: white">
                <?php echo get_field("bottom_tekst_levo","options");  ?>
              </p>
            </div>
            
            <div class="col-12 col-lg-3 col-md-3">
              <p style="color: white">
                <?php echo get_field("bottom_tekst_desno","options");  ?>

              </p>
            </div>
            
        </div>



  
        
        
    </div>
</section>

<?php if(   !isset($_COOKIE["ck_confirmed"]) || !($_COOKIE["ck_confirmed"]) ):   ?>

    <div id="cookie-notice" class="cookie-notice">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 left-cookie-text">
                    <?php _e('Spletno mesto uporablja piškotke za zagotavljanje boljše uporabniške izkušnje in spremljanje statistike obiska. Z izborom opcije "strinjam se" se strinjate z uporabo piškotkov.', 'ave_theme_based'); ?>
                    <a href="/o-nas/splosni-pogoji/" class="more"><?php _e('Več o tem', 'ave_theme_based'); ?></a>
                </div>
                <div class="col-sm-2">
                    <button id="cookie-notice-button2" class="btn btn-x btn-gray btn-xs" ><?php _e('Strinjam se', 'ave_theme_based'); ?></button>
                </div>
            </div>
        </div>
    </div>

<?php endif; ?>
  
 <div id="scrollToTop" class="scrollToTop mbr-arrow-up"><a style="text-align: center;"><i class="mbr-arrow-up-icon mbr-arrow-up-icon-cm cm-icon cm-icon-smallarrow-up"></i></a></div>
  
 <script src="/wp-content/themes/enos/assets/web/assets/jquery/jquery.min.js"></script>
 
 <script src="/wp-content/themes/enos/assets/popper/popper.min.js"></script>
 <script src="/wp-content/themes/enos/assets/tether/tether.min.js"></script>
 <script src="/wp-content/themes/enos/assets/bootstrap/js/bootstrap.min.js"></script>
 <!--<script src="/wp-content/themes/enos/assets/smoothscroll/smooth-scroll.js"></script>-->
 

 <script src="/wp-content/themes/enos/assets/dropdown/js/nav-dropdown.js"></script>
 <script src="/wp-content/themes/enos/assets/dropdown/js/navbar-dropdown.js"></script>

 
<script src="/wp-content/themes/enos/assets/touchswipe/jquery.touch-swipe.min.js"></script>



 <script src="/wp-content/themes/enos/assets/parallax/jarallax.min.js"></script>
 <script src="/wp-content/themes/enos/assets/mbr-switch-arrow/mbr-switch-arrow.js"></script>
 <script src="/wp-content/themes/enos/assets/bootstrapcarouselswipe/bootstrap-carousel-swipe.js"></script>
 <script src="/wp-content/themes/enos/assets/mbr-clients-slider/mbr-clients-slider.js"></script>
 <script src="/wp-content/themes/enos/assets/theme/js/script.js"></script>
 <script src="/wp-content/themes/enos/assets/formoid/formoid.min.js"></script>



<?php wp_footer(); ?>


</body>

</html>
