(function ($, root, undefined) {
    $(function () {
        'use strict';

        // SLIDER partners
        $(document).ready(function () {
            console.log('test 1')
            $('#nav-button').click(function () {
                $(this).toggleClass('open');
            });
            var loc = $(location).attr('hash');
            $('.side-link').each(function () {
                if ($(this).attr('href') == loc) {
                    $(this).addClass('active');
                }
            });
        });
        function setCookie(cname, cvalue, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
            var expires = "expires=" + d.toUTCString();
            document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        }
        $('#cookie-notice-button2').on('click', function () {
            console.log("click cookies");
            setCookie("ck_confirmed", "1", 90);
            $("#cookie-notice").addClass("is-hidden");
            return false;
        });

        $('#povprasevanje .wpcf7-submit.gumb').on('click', function () {
            ga('create', 'UA-167862902-2');
                ga('send' , {
                    'hitType': 'event',
                    'eventCategory': 'povprasevanje_klik',
                    'eventAction': 'povprasevanje_klik',
                    'eventLabel': 'povprasevanje_klik  ',
                });
        });
        $('.my-contact #wpcf7-f50-o3 .wpcf7-submit').on('click', function () {
            ga('create', 'UA-167862902-2');
            ga('send' , {
                'hitType': 'event',
                'eventCategory': 'footer_povprasevanje_klik',
                'eventAction': 'footer_povprasevanje_klik',
                'eventLabel': 'footer_povprasevanje_klik  ',
            });
        });
        $('#header-call').on('click', function () {
            console.log('header-call');
            ga('create', 'UA-167862902-2');
            ga('send' , {
                'hitType': 'event',
                'eventCategory': 'header-call',
                'eventAction': 'header-call',
                'eventLabel': 'header-call  ',
            });
        });

        $('#header-mail').on('click', function () {
            console.log('mail');
            ga('create', 'UA-167862902-2');
            ga('send' , {
                'hitType': 'event',
                'eventCategory': 'header-mail',
                'eventAction': 'header-mail',
                'eventLabel': 'header-mail',
            });
        });
        $('#header-location').on('click', function () {
            console.log('mail');
            ga('create', 'UA-167862902-2');
            ga('send' , {
                'hitType': 'event',
                'eventCategory': 'header-location',
                'eventAction': 'header-location',
                'eventLabel': 'header-location',
            });
        });
        $('.side-link-cenik').on('click', function () {
            $('.side-link-cenik').removeClass('active');
            $(this).addClass('active')
            var selector = $(this).attr("data-id");
            console.log( selector);
            $('.cenik-warp').css("display","none")
            $(selector).css("display","block")
        });

        document.addEventListener( 'wpcf7mailsent', function( event ) {
            window.dataLayer.push({
                "event" : "cf7submission",
                "formId" : event.detail.contactFormId,
                "response" : event.detail.inputs
            })
        });
    });

})(jQuery, this);
